#!/bin/bash

COMMAND=`echo "${1}" | tr -d '[:space:]'`
ARGUMENT_1=`echo "${2}" | tr -d '[:space:]'`
ARGUMENT_2=`echo "${3}" | tr -d '[:space:]'`
ARGUMENT_3=`echo "${4}" | tr -d '[:space:]'`

check_program()
{
	which "${1}" 2>/dev/null 1>/dev/null >/dev/null
	if [ $? -ne 0 ]; then
		echo "Please install '${1}'." 1>&2 >&2
		return 1
	fi
	return 0
}

command_run()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid program name." \
		     1>&2 >&2
		return 1
	fi
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	make ${ARGUMENT_1}.xr
	if [ $? -ne 0 ]; then
		return 1
	fi
	./${ARGUMENT_1}.xr
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_debug()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid program name." \
		     1>&2 >&2
		return 1
	fi
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	check_program "gdb"
	if [ $? -ne 0 ]; then
		return 1
	fi
	make ${ARGUMENT_1}.xd
	if [ $? -ne 0 ]; then
		return 1
	fi
	gdb ./${ARGUMENT_1}.xd
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_sanitize()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid program name." \
		     1>&2 >&2
		return 1
	fi
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	make ${ARGUMENT_1}.xsa
	if [ $? -ne 0 ]; then
		return 1
	fi
	ASAN_OPTIONS="detect_leaks=1" ./${ARGUMENT_1}.xsa
	if [ $? -ne 0 ]; then
		return 1
	fi
	make ${ARGUMENT_1}.xsu
	if [ $? -ne 0 ]; then
		return 1
	fi
	UBSAN_OPTIONS="print_stacktrace=1" ./${ARGUMENT_1}.xsu
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_profile()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid program name." \
		     1>&2 >&2
		return 1
	fi
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	check_program "gprof"
	if [ $? -ne 0 ]; then
		return 1
	fi
	make ${ARGUMENT_1}.xp
	if [ $? -ne 0 ]; then
		return 1
	fi
	./${ARGUMENT_1}.xp
	if [ $? -ne 0 ]; then
		return 1
	fi
	gprof -b ${ARGUMENT_1}.xp gmon.out
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_coverage()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid program name." \
		     1>&2 >&2
		return 1
	fi
	if [ "${ARGUMENT_2}" == "" ]; then
		echo "Invalid second argument, must be a valid source name." \
		     1>&2 >&2
		return 1
	fi
	if [ "${ARGUMENT_3}" != "" ] && [ "${ARGUMENT_3}" != "0" ] && \
	   [ "${ARGUMENT_3}" != "1" ]; then
		echo "Invalid third argument, must be a 0 or 1." 1>&2 >&2
		return 1
	fi
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	check_program "gcov"
	if [ $? -ne 0 ]; then
		return 1
	fi
	if [ "${ARGUMENT_3}" == "1" ]; then
		local OPTIONS="-tfjk"
	else
		local OPTIONS="-nfjk"
	fi
	make ${ARGUMENT_1}.xc
	if [ $? -ne 0 ]; then
		return 1
	fi
	./${ARGUMENT_1}.xc
	if [ $? -ne 0 ]; then
		return 1
	fi
	gcov ${OPTIONS} ${ARGUMENT_2}.gcda
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_clean()
{
	check_program "make"
	if [ $? -ne 0 ]; then
		return 1
	fi
	make clean
	if [ $? -ne 0 ]; then
		return 1
	fi
	rm -rf *.gcda *.gcno *.gcov gmon.out
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_lint()
{
	if [ "${ARGUMENT_1}" == "" ]; then
		echo "Invalid first argument, must be a valid source name." \
		     1>&2 >&2
		return 1
	fi
	check_program "clang-tidy"
	if [ $? -ne 0 ]; then
		return 1
	fi
	clang-tidy --checks="*" "${ARGUMENT_1}" --extra-arg="-xc"             \
	           --extra-arg="-std=c89" --extra-arg="-pedantic-errors"      \
	           --extra-arg="-Weverything"
	if [ $? -ne 0 ]; then
		return 1
	fi
	return 0
}

command_help()
{
	echo -ne "Usage: ./project.sh [command] [arguments] ...\n"
	echo -ne "\n"
	echo -ne "This script manages the current project.\n"
	echo -ne "\n"
	echo -ne "\tcommand:\n"
	echo -ne "\t\thelp       - This help menu.\n"
	echo -ne "\t\trun        - Build and run a release copy of the " \
	         "given program in argument 1.\n"
	echo -ne "\t\tdebug      - Build a debug copy of the given program " \
	         "in argument 1 and debug it.\n"
	echo -ne "\t\tsanitize   - Build sanitizer copies of the given " \
	         "program in argument 1 and run them.\n"
	echo -ne "\t\tprofile    - Build a profiler copy of the given " \
	         "program in argument 1 and run it.\n"
	echo -ne "\t\tcoverage   - Build a coverage copy of the given " \
	         "program in argument 1 and run it using the provided " \
	         "source in argument 2 (set argument 3 to '1' to show " \
	         "full coverage details).\n"
	echo -ne "\t\tclean      - Clean up all build files and temporary " \
	         "files.\n"
	echo -ne "\t\tlint       - Lint the given source file in argument " \
	         "1.\n"
}

case "${COMMAND}" in
	'run')
		command_run
		;;
	'debug')
		command_debug
		;;
	'sanitize')
		command_sanitize
		;;
	'profile')
		command_profile
		;;
	'coverage')
		command_coverage
		;;
	'clean')
		command_clean
		;;
	'lint')
		command_lint
		;;
	*)
		command_help
		;;
esac

exit $?
