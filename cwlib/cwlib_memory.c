/*
	This is free and unencumbered software released into the public domain.
	
	Anyone is free to copy, modify, publish, use, compile, sell, or
	distribute this software, either in source code form or as a compiled
	binary, for any purpose, commercial or non-commercial, and by any
	means.
	
	In jurisdictions that recognize copyright laws, the author or authors
	of this software dedicate any and all copyright interest in the
	software to the public domain. We make this dedication for the benefit
	of the public at large and to the detriment of our heirs and
	successors. We intend this dedication to be an overt act of
	relinquishment in perpetuity of all present and future rights to this
	software under copyright law.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
	EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
	MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
	IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
	OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
	ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
	OTHER DEALINGS IN THE SOFTWARE.
	
	For more information, please refer to <http://unlicense.org/>
*/

/*
	This header provides constants that indicate the return value of
	any cwlib API error.
*/

#include "cwlib_memory.h"

void
cwlib_memory_clear(void *const destination, const cwlib_sz data_size,
                   const cwlib_sz count)
{
	unsigned char *data;
	unsigned char *end;
	CWLIB_ASSERT(destination != NULL);
	CWLIB_ASSERT(data_size > 0);
	CWLIB_ASSERT(count > 0);
	for (data = destination, end = data + (data_size * count);
	     data < end; ++data) {
		*data = 0;
	}
}

void
cwlib_memory_copy(void *CWLIB_RESTRICT destination,
                  const void *CWLIB_RESTRICT source,
                  const cwlib_sz data_size, const cwlib_sz count)
{
	unsigned char *CWLIB_RESTRICT dest_data;
	const unsigned char *CWLIB_RESTRICT src_data;
	const unsigned char *CWLIB_RESTRICT src_end;
	CWLIB_ASSERT(destination != NULL);
	CWLIB_ASSERT(source != NULL);
	CWLIB_ASSERT(data_size > 0);
	CWLIB_ASSERT(count > 0);
	for (dest_data = destination, src_data = source,
	     src_end = src_data + (data_size * count); src_data < src_end;
	     ++dest_data, ++src_data) {
		*dest_data = *src_data;
	}
}

int
cwlib_memory_compare(const void *left, const void *right,
                     const cwlib_sz data_size, const cwlib_sz count)
{
	const unsigned char *left_data;
	const unsigned char *left_end;
	const unsigned char *right_data;
	CWLIB_ASSERT(left != NULL);
	CWLIB_ASSERT(right != NULL);
	CWLIB_ASSERT(data_size > 0);
	CWLIB_ASSERT(count > 0);
	for (left_data = left, left_end = left_data + (data_size * count),
	     right_data = right; left_data < left_end; ++left_data,
	     ++right_data) {
		if (*left_data < *right_data) {
			return -1;
		}
		if (*left_data > *right_data) {
			return 1;
		}
	}
	return 0;
}
