.POSIX:
.SUFFIXES:
.SILENT: help

CC      = gcc
CFLAGS  = -Wall -Wextra -pedantic-errors
LDFLAGS =
LDLIBS  =

all: main.xd

main.xd: main.od cwlib.ad
main.xr: main.or cwlib.ar
main.xsa: main.osa cwlib.asa
main.xsu: main.osu cwlib.asu
main.xc: main.oc cwlib.ac
main.xp: main.op cwlib.ap

test_cwlib_memory.xd: test_cwlib_memory.od cwlib.ad
test_cwlib_memory.xr: test_cwlib_memory.or cwlib.ar
test_cwlib_memory.xsa: test_cwlib_memory.osa cwlib.asa
test_cwlib_memory.xsu: test_cwlib_memory.osu cwlib.asu
test_cwlib_memory.xc: test_cwlib_memory.oc cwlib.ac
test_cwlib_memory.xp: test_cwlib_memory.op cwlib.ap

cwlib.ad: cwlib_error.od cwlib_memory.od
	ar rcs $@ $^
cwlib.ar: cwlib_error.or cwlib_memory.or
	ar rcs $@ $^
cwlib.asa: cwlib_error.osa cwlib_memory.osa
	ar rcs $@ $^
cwlib.asu: cwlib_error.osu cwlib_memory.osu
	ar rcs $@ $^
cwlib.ac: cwlib_error.oc cwlib_memory.oc
	ar rcs $@ $^
cwlib.ap: cwlib_error.op cwlib_memory.op
	ar rcs $@ $^

clean:
	rm -rf *.od *.xd *.or *.xr *.osa *.xsa *.osu *.xsu *.oc *.xc *.op     \
	       *.xp *.ad *.ar *.asa *.asu *.ac *.ap

.SUFFIXES: .c .od .xd .or .xr .osa .xsa .osu .xsu .oc .xc .op .xp
.c.od:
	$(CC) -g -Og -c $(CFLAGS) -o $@ $<
.od.xd:
	$(CC) $(LDFLAGS) -g -Og -o $@ $^ $(LDLIBS)
.c.or:
	$(CC) -O2 -c $(CFLAGS) -o $@ $<
.or.xr:
	$(CC) $(LDFLAGS) -O2 -o $@ $^ $(LDLIBS)
.c.osa:
	$(CC) -c $(CFLAGS) -g -Og -fsanitize=address,leak                     \
	      -fno-omit-frame-pointer -o $@ $<
.osa.xsa:
	$(CC) $(LDFLAGS) -g -Og -fsanitize=address,leak                       \
	      -fno-omit-frame-pointer -o $@ $^ $(LDLIBS)
.c.osu:
	$(CC) -c $(CFLAGS) -g -Og -fsanitize=undefined                        \
	      -fno-omit-frame-pointer -o $@ $<
.osu.xsu:
	$(CC) $(LDFLAGS) -g -Og -fsanitize=undefined -fno-omit-frame-pointer  \
	      -o $@ $^ $(LDLIBS)
.c.oc:
	$(CC) -c $(CFLAGS) -g -O0 -fprofile-arcs -ftest-coverage -o $@ $<
.oc.xc:
	$(CC) $(LDFLAGS) -g -O0 -fprofile-arcs -ftest-coverage                \
	      -o $@ $^ $(LDLIBS) -lgcov
.c.op:
	$(CC) -c $(CFLAGS) -pg -o $@ $<
.op.xp:
	$(CC) $(LDFLAGS) -Og -pg -o $@ $^ $(LDLIBS)
