/*
	This header provides standard functionality for the base cwlib
	library. Some of this is useful outside the library for normal users
	as it allows the use of some functionality that is not available in
	all standards, such as inlining or restricting. Depending upon your
	development environment and architecture, some configuration will
	be required.
	
	The following preprocessor macro's may be defined to alter the
	codes functionality:
	
		CWLIB_SHARED - If defined, exports code to build a shared
		               library.
		CWLIB_DEBUG  - If defined, CWL_ASSERT will be enabled.
		CWLIB_S8     - Set to a 8-bit signed integer type.
		CWLIB_U8     - Set to a 8-bit unsigned integer type.
		CWLIB_S16    - Set to a 16-bit signed integer type.
		CWLIB_U16    - Set to a 16-bit unsigned integer type.
		CWLIB_S32    - Set to a 32-bit signed integer type.
		CWLIB_U32    - Set to a 32-bit unsigned integer type.
		CWLIB_S64    - Set to a 64-bit signed integer type.
		CWLIB_U64    - Set to a 64-bit unsigned integer type.
		CWLIB_SZ     - Set to the largest unsigned integer type.
		CWLIB_R32    - Set to a 32-bit floating type.
		CWLIB_R64    - Set to a 64-bit floating type.
		CWLIB_API    - Specifies the API for this library.
		CWLIB_CALL   - Specifies the calling convention of this
		               library.
	
	The syntax provided by this header is:
	
		CWLIB_INLINE     - Used to provid standard inline
		                   functionality.
		CWLIB_RESTRICT   - Used to provid standard restrict
		                   functionality.
		CWLIB_UNUSED     - Used to mark code that is unused.
		CWLIB_DEPRECATED - Used to mark code as deprecated.
	
	The following function macro's are provided:
	
		CWLIB_STATIC_ASSERT - Asserts a given condition at compile
		                      time.
		CWLIB_ASSERT        - Asserts a given condition at runtime.
		                      Note that this is only enabled if the
		                      CWLIB_DEBUG macro is defined.
*/

#ifndef CWLIB_H
#define CWLIB_H

#if defined(__GNUC__) || defined(__clang__)
#	define CWLIB_INLINE __inline__
#elif defined(_MSC_VER) || defined(__ICL) || defined(__BORLANDC__) ||         \
      (defined(__WATCOMC__) && __WATCOMC__ >= 1100)
#	define CWLIB_INLINE __inline
#elif defined(__cplusplus) || defined(__INTEL_COMPILER) ||                    \
      (defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L)
#	define CWLIB_INLINE inline
#else
#	define CWLIB_INLINE
#endif

#if defined(__GNUC__) || defined(__clang__)
#	define CWLIB_RESTRICT __restrict__
#elif defined(_MSC_VER)
#	define CWLIB_INLINE __restrict
#elif !defined(__cplusplus) && (defined(__STDC_VERSION__) &&                  \
      __STDC_VERSION__ >= 199901L)
#	define CWLIB_INLINE inline
#else
#	define CWLIB_INLINE
#endif

#ifdef CWLIB_DEBUG
#	if __GNUC__
#		define CWLIB_ASSERT(CONDITION)                                \
			do {                                                  \
				if (!(CONDITION)) {                           \
					__builtin_trap();                     \
				}                                             \
			} while(0)
#	elif _MSC_VER
#		define CWLIB_ASSERT(CONDITION)                                \
			do {                                                  \
				if (!(CONDITION)) {                           \
					__debugbreak();                       \
				}                                             \
			} while(0)
#	else
#		define CWLIB_ASSERT(CONDITION)                                \
			do {                                                  \
				if (!(CONDITION)) {                           \
					*(volatile int *)0 = 0;               \
				}                                             \
			} while(0)
#	endif
#else
#	define CWLIB_ASSERT(CONDITION)
#endif

#if defined(__GNUC__) && defined(__GNUC_MINOR__) &&                           \
    defined(__GNUC_PATCHLEVEL__) && ((__GNUC__ * 10000 + __GNUC_MINOR__       \
    * 100 + __GNUC_PATCHLEVEL__) >= 30100)
#	define CWLIB_DEPRECATED __attribute__((deprecated))
#else
#	define CWLIB_DEPRECATED
#endif

#if defined(__GNUC__) || defined(__clang__)
#	define CWLIB_UNUSED __attribute__((unused))
#else
#	define CWLIB_UNUSED
#endif

#define CWLIB_STATIC_ASSERT(NAME, CONDITION)                                  \
	typedef int CWLIB_STATIC_ASSERT_##NAME[(CONDITION) * 2 - 1]

#ifdef CWLIB_S8
typedef CWLIB_S8 cwlib_s8;
#else
typedef signed char cwlib_s8;
#endif /* CWLIB_S8 */

#ifdef CWLIB_U8
typedef CWLIB_U8 cwlib_u8;
#else
typedef unsigned char cwlib_u8;
#endif /* CWLIB_U8 */

#ifdef CWLIB_S16
typedef CWLIB_S16 cwlib_s16;
#else
typedef signed short cwlib_s16;
#endif /* CLWIB_S16 */

#ifdef CWLIB_U16
typedef CWLIB_U16 cwlib_u16;
#else
typedef unsigned short cwlib_u16;
#endif /* CLWIB_U16 */

#ifdef CWLIB_S32
typedef CWLIB_S32 cwlib_s32;
#else
typedef signed int cwlib_s32;
#endif /* CWLIB_S32 */

#ifdef CWLIB_U32
typedef CWLIB_U32 cwlib_u32;
#else
typedef unsigned int cwlib_u32;
#endif /* CWLIB_U32 */

#ifdef CWLIB_S64
typedef CWLIB_S64 cwlib_s64;
#else
typedef signed long cwlib_s64;
#endif /* CWLIB_S64 */

#ifdef CWLIB_U64
typedef CWLIB_U64 cwlib_u64;
#else
typedef unsigned long cwlib_u64;
#endif /* CWLIB_U64 */

#ifdef CWLIB_SZ
typedef CWLIB_SZ cwlib_sz;
#else
typedef unsigned long cwlib_sz;
#endif /* CWLIB_SZ */

#ifdef CWLIB_R32
typedef CWLIB_R32 cwlib_r32;
#else
typedef float cwlib_r32;
#endif /* CWLIB_R32 */

#ifdef CWLIB_R64
typedef CWLIB_R64 cwlib_r64;
#else
typedef double cwlib_r64;
#endif /* CWLIB_R64 */

CWLIB_STATIC_ASSERT(cwlib_s8, sizeof(cwlib_s8) == 1);
CWLIB_STATIC_ASSERT(cwlib_u8, sizeof(cwlib_u8) == 1);
CWLIB_STATIC_ASSERT(cwlib_s16, sizeof(cwlib_s16) == 2);
CWLIB_STATIC_ASSERT(cwlib_u16, sizeof(cwlib_u16) == 2);
CWLIB_STATIC_ASSERT(cwlib_s32, sizeof(cwlib_s32) == 4);
CWLIB_STATIC_ASSERT(cwlib_u32, sizeof(cwlib_u32) == 4);
CWLIB_STATIC_ASSERT(cwlib_s64, sizeof(cwlib_s64) == 8);
CWLIB_STATIC_ASSERT(cwlib_u64, sizeof(cwlib_u64) == 8);
CWLIB_STATIC_ASSERT(cwlib_sz, sizeof(cwlib_sz) == 8);
CWLIB_STATIC_ASSERT(cwlib_r32, sizeof(cwlib_r32) == 4);
CWLIB_STATIC_ASSERT(cwlib_r64, sizeof(cwlib_r64) == 8);

#ifndef NULL
#	ifdef __cplusplus
#		define NULL 0
#	else
#		define NULL ((void *)0)
#	endif
#endif /* NULL */

#ifdef __cplusplus
#	define CWLIB_API__CPP extern "C"
#else
#	define CWLIB_API__CPP extern
#endif

#ifdef CWLIB_SHARED
#	if defined(_WIN32) || defined(__CYGWIN__) || defined(__OS2__)
#		define CWLIB_API__SHARED __declspec(dllexport)
#	elif defined(__GNUC__) && __GNUC__ >= 4
#		define CWLIB_API__SHARED __attribute__((visibility("default")))
#	else
#		define CWLIB_API__SHARED
#	endif
#else
#	define CWLIB_API__SHARED
#endif

#define CWLIB_API CWLIB_API__CPP CWLIB_API__SHARED

#if defined(_WIN32) && !defined(__GNUC__)
#	define CWLIB_CALL __cdecl
#elif defined(__OS2__)
#	define CWLIB_CALL _System
#else
#	define CWLIB_CALL
#endif

#endif /* CWLIB_H */
