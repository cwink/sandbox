/*
	This header provides macro's to assist in writing unit tests. Notice
	that this header (unlike cwlib itself) requires the standard C
	library.

CWLIB_TEST_DEFINE(test_memory_clear)
{
	int a[4];
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	cwlib_memory_clear(a, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 3 && a[3] == 4);
	cwlib_memory_clear(a + 2, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0);
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	cwlib_memory_clear(a, sizeof(int), 4);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0);
}

CWLIB_TEST_MAIN()
{
	CWLIB_TEST_INITIALIZE(cwlib_memory);
	CWLIB_TEST_CALL(test_memory_clear);
	CWLIB_TEST_TERMINATE();
}
*/

#ifndef CWLIB_TEST_H
#define CWLIB_TEST_H

#include <stdio.h>
#include <stdlib.h>

#define CWLIB_TEST_MAIN()                                                     \
	int                                                                   \
	main(void)

#define CWLIB_TEST_INITIALIZE(NAME)                                           \
	const char *cwlib_test_name;                                          \
	cwlib_sz cwlib_test_total_passes;                                     \
	cwlib_sz cwlib_test_total_failures;                                   \
	cwlib_test_name = #NAME;                                              \
	cwlib_test_total_passes = 0;                                          \
	cwlib_test_total_failures = 0;                                        \
	fprintf(stdout, "Testing module '%s'...\n", cwlib_test_name)

#define CWLIB_TEST_TERMINATE()                                                \
	fprintf(stdout, "Finished testing module '%s' with %lu passes and "   \
	        "%lu failures.\n", cwlib_test_name, cwlib_test_total_passes,  \
	        cwlib_test_total_failures);                                   \
	return cwlib_test_total_failures > 0 ? EXIT_FAILURE : EXIT_SUCCESS

#define CWLIB_TEST_CALL(NAME)                                                 \
	fprintf(stdout, "\tRunning test '%s'...\n", #NAME);                   \
	NAME(&cwlib_test_total_passes, &cwlib_test_total_failures);           \
	fprintf(stdout, "\tFinished running test '%s'.\n", #NAME)

#define CWLIB_TEST_DEFINE(NAME)                                               \
	static void                                                           \
	NAME(cwlib_sz *const CWLIB_RESTRICT cwlib_test_total_passes,          \
	     cwlib_sz *const CWLIB_RESTRICT cwlib_test_total_failures)

#define CWLIB_TEST_ASSERT(CONDITION)                                          \
	do {                                                                  \
		if (CONDITION) {                                              \
			fprintf(stdout, "\t\t[%u] PASS: %s\n", __LINE__,      \
			        #CONDITION);                                  \
			++*cwlib_test_total_passes;                           \
			break;                                                \
		}                                                             \
		fprintf(stderr, "\t\t[%u] FAIL: %s\n", __LINE__,              \
		        #CONDITION);                                          \
		++*cwlib_test_total_failures;                                 \
	} while(0)

#endif /* CWLIB_TEST_H */
