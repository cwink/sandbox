#include "cwlib_memory.h"
#include "cwlib_test.h"

#include <stdio.h>
#include <stdlib.h>

CWLIB_TEST_DEFINE(test_memory_clear)
{
	int a[4];
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	cwlib_memory_clear(a, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 3 && a[3] == 4);
	cwlib_memory_clear(a + 2, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0);
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	cwlib_memory_clear(a, sizeof(int), 4);
	CWLIB_TEST_ASSERT(a[0] == 0 && a[1] == 0 && a[2] == 0 && a[3] == 0);
}

CWLIB_TEST_DEFINE(test_memory_copy)
{
	int a[4];
	int b[4];
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	b[0] = 0;
	b[1] = 0;
	b[2] = 0;
	b[3] = 0;
	cwlib_memory_copy(b, a, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 1 && a[1] == 2 && a[2] == 3 && a[3] == 4 &&
	                  b[0] == 1 && b[1] == 2 && b[2] == 0 && b[3] == 0);
	cwlib_memory_copy(b + 2, a + 2, sizeof(int), 2);
	CWLIB_TEST_ASSERT(a[0] == 1 && a[1] == 2 && a[2] == 3 && a[3] == 4 &&
	                  b[0] == 1 && b[1] == 2 && b[2] == 3 && b[3] == 4);
	b[0] = 0;
	b[1] = 0;
	b[2] = 0;
	b[3] = 0;
	cwlib_memory_copy(b, a, sizeof(int), 4);
	CWLIB_TEST_ASSERT(a[0] == 1 && a[1] == 2 && a[2] == 3 && a[3] == 4 &&
	                  b[0] == 1 && b[1] == 2 && b[2] == 3 && b[3] == 4);
}

CWLIB_TEST_DEFINE(test_memory_compare)
{
	int a[4];
	int b[4];
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 4;
	b[0] = 1;
	b[1] = 2;
	b[2] = 3;
	b[3] = 4;
	CWLIB_TEST_ASSERT(cwlib_memory_compare(a, b, sizeof(int), 4) == 0);
	a[0] = 1;
	a[1] = 2;
	a[2] = 3;
	a[3] = 2;
	CWLIB_TEST_ASSERT(cwlib_memory_compare(a, b, sizeof(int), 4) == -1);
	a[0] = 1;
	a[1] = 2;
	a[2] = 6;
	a[3] = 4;
	CWLIB_TEST_ASSERT(cwlib_memory_compare(a, b, sizeof(int), 4) == 1);
}

CWLIB_TEST_MAIN()
{
	CWLIB_TEST_INITIALIZE(cwlib_memory);
	CWLIB_TEST_CALL(test_memory_clear);
	CWLIB_TEST_CALL(test_memory_copy);
	CWLIB_TEST_CALL(test_memory_compare);
	CWLIB_TEST_TERMINATE();
}
