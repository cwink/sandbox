#!/bin/bash

MODULE=`echo "${1}" | tr -d '[:space:]'`
DETAILED=`echo "${2}" | tr -d '[:space:]'`
SOURCE="cwlib_${MODULE}"
PROGRAM="test_${SOURCE}"

if [ "${MODULE}" == "" ]; then
	echo -ne "Usage: ./run_test.sh [module] [detailed]\n"
	echo -ne "\n"
	echo -ne "This script is used to run tests for a single provided " \
	         "cwlib module.\n"
	echo -ne "\n"
	echo -ne "\tmodule   - The module to run, i.e. to run a test for " \
	         "the 'cwlib_memory.h/cwlib_memory.c' code, use 'memory'.\n"
	echo -ne "\tdetailed - Pass '1' to this argument to show more " \
	         "detailed information on the test.\n"
	exit 0
fi
./project.sh sanitize "${PROGRAM}"
if [ $? -ne 0 ]; then
	exit 1
fi
./project.sh profile "${PROGRAM}"
if [ $? -ne 0 ]; then
	exit 1
fi
./project.sh coverage "${PROGRAM}" "${SOURCE}" "${DETAILED}"
if [ $? -ne 0 ]; then
	exit 1
fi

exit 0
