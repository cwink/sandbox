#ifndef CWLIB_MEMORY_H
#define CWLIB_MEMORY_H

#include "cwlib.h"

struct cwlib_memory_allocator {
	void *(*malloc)(cwlib_sz);
	void *(*realloc)(void *, cwlib_sz);
	void (*free)(void *);
};

CWLIB_API void CWLIB_CALL
cwlib_memory_clear(void *destination, cwlib_sz data_size, cwlib_sz count);

CWLIB_API void CWLIB_CALL
cwlib_memory_copy(void *CWLIB_RESTRICT destination,
                  const void *CWLIB_RESTRICT source, cwlib_sz data_size,
                  cwlib_sz count);

CWLIB_API int CWLIB_CALL
cwlib_memory_compare(const void *left, const void *right,
                     cwlib_sz data_size, cwlib_sz count);

#endif /* CWLIB_MEMORY_H */
