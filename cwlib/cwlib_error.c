#include "cwlib_error.h"

const char *
cwlib_error_string(int code) {
	switch (code) {
	case CWLIB_ERROR_NONE:
		return "no error has occured";
	case CWLIB_ERROR_ALLOCATION:
		return "memory allocation has failed";
	}
	return "invalid error code";
}
