/*
	This header file provides macro's defining various error's signaled
	in this library. Note that they all are standard integers, so testing
	against an error would be performed like so:
	
		int error_code;
		if ((error_code = cwlib_foobar("hello")) < 0) {
			fprintf(stderr, "ERROR: %s.\n",
			        cwlib_error_string(error_code));
			return EXIT_FAILURE;
		}
*/

#ifndef CWLIB_ERROR_H
#define CWLIB_ERROR_H

#include "cwlib.h"

#define CWLIB_ERROR_NONE       0
#define CWLIB_ERROR_ALLOCATION 1

CWLIB_API const char * CWLIB_CALL
cwlib_error_string(int code);

#endif /* CWLIB_ERROR_H */
