set -o vi

alias system_update='/usr/local/src/dev-setup/ubuntu_update.sh'
alias ll='ls -ltr'
alias vim='vim.nox'
alias vi='vim.nox'

export EDITOR=vim
