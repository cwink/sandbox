#!/bin/bash

sudo apt -y clean update

if [ $? -ne 0 ]; then
	echo "Failed to update aptitude repository." 1>&2 >&2
	exit 1
fi

sudo apt -y upgrade

if [ $? -ne 0 ]; then
	echo "Failed to upgrade packages." 1>&2 >&2
	exit 2
fi

sudo apt-get -y --purge autoremove

if [ $? -ne 0 ]; then
	echo "Failed to purge old packages." 1>&2 >&2
	exit 3
fi

sudo apt -y clean

if [ $? -ne 0 ]; then
	echo "Failed to clean aptitude repository." 1>&2 >&2
	exit 4
fi

exit 0
