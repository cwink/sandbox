#!/bin/bash

FILE="${1}"

if [ ! -e "${FILE}" ]; then
	echo "File '${FILE}' does not exist." 1>&2 >&2
	exit 1
fi

which gcc 2>/dev/null 1>/dev/null >/dev/null

if [ $? -ne 0 ]; then
	echo "The program 'gcc' is required." 1>&2 >&2
	exit 2
fi

which sed 2>/dev/null 1>/dev/null >/dev/null

if [ $? -ne 0 ]; then
	echo "The program 'sed' is required." 1>&2 >&2
	exit 3
fi

which sed 2>/dev/null 1>/dev/null >/dev/null

if [ $? -ne 0 ]; then
	echo "The program 'ctags' is required." 1>&2 >&2
	exit 4
fi

FILE_NO_EXT="${FILE%%.*}"

gcc -M "${FILE}" | sed -e "s/${FILE}//g"                           \
                       -e "s/${FILE_NO_EXT}\\.o//g"                \
                       -e 's/^\(\s\|\t\)*://g'                     \
                       -e 's/\\//g'                                \
                       -e 's/^\(\s\|\t\)*//g'                      \
                       -e 's/\(\s\|\t\)*$//g'                      \
                       -e 's/\(\s\|\t\)/\n/g'                      \
                 | ctags -L - --languages="+c"                     \
                         --c-kinds="-lv+defgmpstux" --fields="+Sn" \
                         -R -a

exit 0
