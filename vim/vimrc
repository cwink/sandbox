set nocompatible
filetype plugin indent on
syntax on

if !isdirectory($HOME . "/.vim/swap")
	call mkdir($HOME . "/.vim/swap", "p")
endif

set directory=$HOME/.vim/swap
set swapfile

if !isdirectory($HOME . "/.vim/backup")
	call mkdir($HOME . "/.vim/backup", "p")
endif

set backupdir=$HOME/.vim/backup
set backup

if !isdirectory($HOME . "/.vim/undo")
	call mkdir($HOME . "/.vim/undo", "p")
endif

set undodir=$HOME/.vim/undo
set undofile

if !isdirectory($HOME . "/.vim/colors")
	call mkdir($HOME . "/.vim/colors", "p")
endif

if filereadable($HOME . "/.vim/colors/solarized.vim")
	syntax enable
	set background=dark
	colorscheme solarized
endif

packadd termdebug
let g:termdebug_wide=1
set viminfofile=$HOME/.vim/viminfo
set ts=4 sw=4 sts=4 smarttab expandtab
set invlist
set hidden
set path+=**
set number
let g:netrw_banner=0
let g:netrw_browse_split=4
let g:netrw_altv=1
let g:netrw_liststyle=3
let g:netrw_list_hide=netrw_gitignore#Hide()
let g:netrw_list_hide.=',\(^\|\s\s\)\zs\.\S\+'
let g:netrw_winsize=25
hi! link netrwMarkFile Search

if has('win32')
	let g:netrw_localcopydircmd='xcopy /E/H'
else
	let g:netrw_localcopydircmd='cp -r'
endif

inoremap <C-U> <C-G>u<C-U>
inoremap <C-W> <C-G>u<C-W>
nnoremap <silent> <C-L> :nohlsearch<C-R>=has('diff')?'<Bar>diffupdate':''<CR><CR><C-L>
nnoremap <silent> gw :wa!<CR>
nnoremap <silent> gq :qa!<CR>
nnoremap <silent> gd :bd!<CR>
nnoremap <silent> < :bp!<CR>
nnoremap <silent> > :bn!<CR>
nnoremap <C-N> :Lexplore<CR>
nnoremap <C-Q> :cw<CR>

augroup C
	autocmd!
	autocmd BufNewFile,BufRead,BufEnter *.c,*.h set filetype=c
	autocmd FileType c set omnifunc=syntaxcomplete#Complete
	autocmd FileType c set completeopt+=menuone
	autocmd FileType c set completeopt+=popup
	autocmd FileType c set ts=8 sw=8 sts=8 smarttab noexpandtab
augroup END

augroup VIMSCRIPT
	autocmd!
	autocmd FileType vim set omnifunc=syntaxcomplete#Complete
	autocmd FileType vim set completeopt+=menuone
	autocmd FileType vim set completeopt+=popup
	autocmd FileType vim set ts=8 sw=8 sts=8 smarttab noexpandtab
augroup END

augroup SHELL
	autocmd!
	autocmd FileType sh set omnifunc=syntaxcomplete#Complete
	autocmd FileType sh set completeopt+=menuone
	autocmd FileType sh set completeopt+=popup
	autocmd FileType sh set ts=8 sw=8 sts=8 smarttab noexpandtab
augroup END

augroup MAKEFILE
	autocmd!
	autocmd FileType make set ts=8 sw=8 sts=8
augroup END
