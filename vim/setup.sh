#!/bin/bash

COMMAND="${1}"

case "${COMMAND}" in
	"install")
		mkdir -p "${HOME}"/.vim/{swap,undo,backup}
		ln -s "${PWD}/vimrc" "${HOME}/.vimrc"
		ln -s "${PWD}/plugin" "${HOME}/.vim/plugin"
		ln -s "${PWD}/colors" "${HOME}/.vim/colors"
		;;
	"destroy")
		rm -rf "${HOME}/.viminfo"
		rm -rf "${HOME}/.vimrc"
		rm -rf "${HOME}/.vim"
		;;
	*)
		echo "Usage: ./setup.sh [command]" 2>&1 >&1
		echo "	install - Install and configure vim." 2>&1 >&1
		echo "	destroy - Purge and destroy current vim configuration." 2>&1 >&1
		exit 1
		;;
esac

exit 0
